public class Triangle {
    private Integer x;
    private Integer y;
    private Integer z;

    public Triangle(Integer x, Integer y, Integer z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    boolean isValid() {
        return x + y > z && y + z > x && z + x > y;
    }

    Double area() {
        if (isValid()) {
            double pp = (x + y + z) / 2; // pusperimetrs
            return Math.sqrt(pp * (pp - x) * (pp - y) * (pp - z));
        } else {
            return null;
        }
    }
}
