import java.util.Scanner;

public class TriangleExercise {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input Side-1: ");
        int side1 = scanner.nextInt();
        System.out.print("Input Side-2: ");
        int side2 = scanner.nextInt();
        System.out.print("Input Side-3: ");
        int side3 = scanner.nextInt();
        Triangle triangle = new Triangle(side1, side2, side3);
        if (triangle.isValid()) {
            System.out.println("Triangle is valid");
        } else {
            System.out.println("Triangle is invalid");
        }
    }

}