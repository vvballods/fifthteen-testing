import org.junit.jupiter.api.*;

public class Testing {

    @BeforeEach
    void beforeEach() {
        System.out.println("Before Each");
    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("Before All");
    }

    @AfterEach
    void afterEach() {
        System.out.println("After Each");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("After All");
    }

    @Test
    @DisplayName("Valtera tests")
    void firstTest() {
        System.out.println("Test");
    }

    @Test
    void secondTest() {
        System.out.println("Test");
    }
}
