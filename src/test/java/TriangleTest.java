
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TriangleTest {

    @Test
    void testIfTriangleIsValid() {
        // given
        Triangle triangle = new Triangle(1, 1, 1);
        // when
        boolean result = triangle.isValid();
        // then
        assertThat(result).isTrue();
    }

    @Test
    void testTriangleArea() {
        Triangle triangle = new Triangle(3, 4, 5);

        Double area = triangle.area();

        assertThat(area).isEqualTo(6.0);
    }

    @Test
    void testNonValidTriangleArea() {
        Triangle triangle = new Triangle(1, 2, 5);

        Double area = triangle.area();

        assertThat(area).isNull();
    }
}
